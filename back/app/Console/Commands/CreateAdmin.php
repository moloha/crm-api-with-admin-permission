<?php

namespace App\Console\Commands;


use App\Http\Library\ApiHelpers;
use App\Models\User;
use Illuminate\Console\Command;
//use Illuminate\Support\Facades\Hash;
use Validator;



class CreateAdmin extends Command
{

    use ApiHelpers;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:admin';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create user with admin permission';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {


        $name = $this->ask('name');

            //запрашиваем email адрес
        $email =$this->ask('email');

            //запрашиваю пароль
        $password = $this->ask('password');

        //валидация
        $args = [
            'name'=>$name,
            'email'=>$email,
            'password'=>$password
        ];
//          может быть альтернативой общей валидации, но необходимо прописывать для каждого поля нуждающегося в валидации
//        $validator = Validator::make(['password'=>$password],['password' => 'required|string|min:4']);
        $validator = Validator::make($args,$this->userValidatedRules());
        if ($validator->fails()){
            //выводим ошибку
            $this->error('User not created');

            //выводим ошибки валидации
            foreach ($validator->errors()->all() as $error){
                $this->comment($error);
            }
            die;
        }
        $this->info('name : '.$name);
        $this->info('email : '.$email);
        if ($this->confirm('Is this information correct?')){
//            $user = new User();
//            $user->name =$name;
//            $user->email = $email;
//            $user->password = bcrypt($password);
//            $user->is_admin = 1;
//            $user->save();
            $user = User::create([
                'name'=>$name,
                'email'=>$email,
                'password'=>bcrypt($password),
                'is_admin'=>1,
            ]);
            $this->info("Admin Created.");
        }
        return 0;
    }
}
