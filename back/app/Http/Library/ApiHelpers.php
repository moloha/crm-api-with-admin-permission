<?php

namespace App\Http\Library;

use Illuminate\Http\JsonResponse;
use App\Models\User;
use Illuminate\Http\Request;
use Validator;


trait ApiHelpers
{
    /**
     * @param $user
     *
     * @return bool
     */
    protected function isAdmin($user): bool
    {
        if (!empty($user)) {
            if ($user['is_admin']){
                return $user->tokenCan('admin');
            }
        }
        return false;
    }

    /**
     * @param $user
     *
     * @return bool
     */
    protected function isSuperuser($user): bool
    {
        if (!empty($user)) {
            if($user['is_superuser']){
                return $user->tokenCan('superuser');
            }
        }
        return false;
    }

    /**
     * @param $user
     *
     * @return bool
     */
    protected function isCustomer($user): bool
    {
        if (!empty($user)) {
            if($user['is_customer']){
                return $user->tokenCan('customer');
            }
        }
        return false;
    }


    /**
     * @param $data
     * @param string $message
     * @param int $code
     *
     * @return JsonResponse
     */
    protected function onSuccess($data, string $message = '', int $code = 200): JsonResponse
    {
        return response()->json([
            'status' => $code,
            'message' => $message,
            'auth_token' => $data,
        ], $code);
    }


    /**
     * @param int $code
     * @param string $message
     *
     * @return JsonResponse
     */
    protected function onError(int $code, string $message = ''): JsonResponse
    {
        return response()->json([
            'status' => $code,
            'message' => $message,
        ], $code);
    }

    /**
     * @return string[]
     */
    protected function taskValidationRules(): array
    {
        return [
            'title' => 'required|string',
            'content' => 'required|string',
        ];
    }


    /**
     * @return string[][]
     */
    protected function userValidatedRules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:4'],
        ];
    }

}
