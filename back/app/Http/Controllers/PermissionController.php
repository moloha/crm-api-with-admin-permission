<?php

namespace App\Http\Controllers;

use App\Http\Library\ApiHelpers;
use App\Models\Task;
use App\Models\User;
use Exception;
use http\Message;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Throwable;

class PermissionController extends Controller
{
    use ApiHelpers;

    public function task(Request $request): JsonResponse
    {
        if ($this->isAdmin($request->user())) {
            $post = DB::table('tasks')->get();
            return $this->onSuccess($post, 'Task Retrieved');
        }
        return $this->onError(401, 'Unauthorized Access');
    }

    public function singleTask(Request $request, $id): JsonResponse
    {
        $user = $request->user();
        if ($this->isAdmin($user) || $this->isSuperuser($user) || $this->isCustomer($user)) {
            $task = DB::table('tasks')->where('id', $id)->first();
            if (!empty($task)) {
                return $this->onSuccess($task, 'Task Retrieved');
            }
            return $this->onError(404, 'Task Not Found');
        }
        return $this->onError(401, 'Unauthorized Access');
    }


    public function createTask(Request $request): JsonResponse
    {
        $user = $request->user();
        if ($this->isAdmin($user) || $this->isSuperuser($user)) {
            $validator = Validator::make($request->all(), $this->taskValidationRules());
            if ($validator->passes()) {
                // Создание нового сообщения
                $task = new Task();
                $task->title = $request->input('title');
                $task->slug = Str::slug($request->input('title'));
                $task->content = $request->input('content');
//                $task->user_id = $request->input('user_id');
                $task->user_id = $request->user()->id;
                $task->save();
                return $this->onSuccess($task, 'Task Created');
            }
            return $this->onError(400, $validator->errors());
        }
        return $this->onError(401, 'Unauthorized Access');
    }


    public function updateTask(Request $request, $id): JsonResponse
    {
        $user = $request->user();
        if ($this->isAdmin($user) || $this->isSuperuser($user)) {
            $validator = Validator::make($request->all(), $this->taskValidationRules());
            if ($validator->passes()) {
                // Обновление сообщения
                $task = Task::find($id);
                $task->title = $request->input('title');
                $task->slug = Str::slug($request->input('title'));
                $task->content = $request->input('content');
                $task->save();
                return $this->onSuccess($task, 'Task Updated');
            }
            return $this->onError(400, $validator->errors());
        }
        return $this->onError(401, 'Unauthorized Access');
    }

    public function deleteTask(Request $request, $id): JsonResponse
    {
        $user = $request->user();
        if ($this->isAdmin($user) || $this->isSuperuser($user)) {
            $task = Task::find($id); // Найдем id сообщения
            $task->delete(); // Удаляем указанное сообщение
            if (!empty($task)) {
                return $this->onSuccess($task, 'Task Deleted');
            }
            return $this->onError(404, 'Task Not Found');
        }
        return $this->onError(401, 'Unauthorized Access');
    }


    public function createSuperuser(Request $request): JsonResponse
    {
        $user = $request->user();
        if ($this->isAdmin($user)) {
            $validator = Validator::make($request->all(), $this->userValidatedRules());
            if ($validator->passes()) {
                // Создаем нового Автора
                User::create([
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'is_superuser' => 1,
                    'password' => Hash::make($request->input('password')),
                ]);
                $superuserToken = $user->createToken('auth_token', ['superuser'])->plainTextToken;
                return $this->onSuccess($superuserToken, 'User Created With Superuser Privilege');
            }
            return $this->onError(400, $validator->errors());
        }
        return $this->onError(401, 'Unauthorized Access');
    }

    public function createCustomer(Request $request): JsonResponse
    {
        $user = $request->user();
        if ($this->isAdmin($user) || $this->isSuperuser($user)) {
            $validator = Validator::make($request->all(), $this->userValidatedRules());
            if ($validator->passes()) {
                // Создаем нового Подписчика
                User::create([
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'is_customer' => 1,
                    'password' => Hash::make($request->input('password')),
                ]);
                $customerToken = $user->createToken('auth_token', ['customer'])->plainTextToken;
                return $this->onSuccess($customerToken, 'User Created With Customer Privilege');
            }
            return $this->onError(400, $validator->errors());
        }
        return $this->onError(401, 'Unauthorized Access');
    }


//    public function deleteSuperuser(Request $request, $id): JsonResponse
//    {
//        $user = $request->user();
//        if ($this->isAdmin($user)) {
//            $user = User::find($id); // Найдем id пользователя
//            if ($user->is_admin !== 1) {
//                if($user->is_customer !== 1){
//                    $user->delete(); // Удалим указанного пользователя
//                    if (!empty($user)) {
//                        return $this->onSuccess('', 'Superuser Deleted');
//                    }
//                    return $this->onError(404, 'Superuser Not Found');
//                }
//                return $this->onError(401, 'Mistake Role Access');
//            }
//            return $this->onError(401, 'Mistake Role Access');
//        }
//        return $this->onError(401, 'Unauthorized Access');
//    }

    public function deleteSuperuser (Request $request, $id): JsonResponse
    {
        $user = $request->user();
        if($this->isAdmin($user)){
            try {
                // Найдем id пользователя
                $superUser = User::findOrFail($id);
            }catch (Throwable $e){
                return response()->json([
                    'message' => 'SuperUser not found.'
                ], 404);
            }
                // Удалим указанного пользователя
            if($superUser->is_superuser){
                $superUser->delete();
                if (!empty($superUser)){
                    return $this->onSuccess('', 'SuperUser Deleted');
                }
                return $this->onError(401, 'SuperUser Not Delete');
            }
            return $this->onError(401, 'Mistake Role Access');
        }
        return $this->onError(401, 'Unauthorized Access');
    }

    public function deleteCustomer(Request $request, $id): JsonResponse
    {
        $user = $request->user();
//        if ($this->isAdmin($user) || $this->isSuperuser($user)) {
//            $user = User::find($id); // Найдем id пользователя
//            if ($user->is_admin !== 1) {
//                if($user->is_superuser !== 1){
//                    $user->delete(); // Удалим указанного пользователя
//                    if (!empty($user)) {
//                        return $this->onSuccess('', 'Customer Deleted');
//                    }
//                    return $this->onError(404, 'Customer Not Found');
//                }
//                return $this->onError(401, 'Mistake Role Access');
//            }
//            return $this->onError(401, 'Mistake Role Access');
//        }
//        return $this->onError(401, 'Unauthorized Access');
        if ($this->isAdmin($user) || $this->isSuperuser($user)){
            try {
                // Найдем id пользователя
                $customer = User::findOrFail($id);
            }catch (Throwable $e){
                return response()->json([
                    'message' => 'Customer not found.'
                ], 404);
            }
                // Удалим указанного пользователя
            if($customer->is_customer){
                $customer->delete();
                if (!empty($customer)) {
                        return $this->onSuccess('', 'Customer Deleted');
                }
                return $this->onError(401, 'Customer Not Delete');
            }
            return $this->onError(401, 'Mistake Role Access');
        }
        return $this->onError(401, 'Mistake Role Access');
    }
}
