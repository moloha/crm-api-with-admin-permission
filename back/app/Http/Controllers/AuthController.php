<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Http\Response;


class AuthController extends Controller
{
    public function register(Request $request)
    {
        $fields = $request->validate([
            'name'=>'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:4',
        ]);

        if($request['is_superuser']==$request['is_customer'])
        {
            return response()->json([
                'message' => 'Invalid permission details'
            ], 401);
        }


        $user = User::create([
            'name'=>$fields['name'],
            'email'=>$fields['email'],
            'password'=>bcrypt($fields['password']),
            'is_superuser'=>$request['is_superuser'],
            'is_customer'=>$request['is_customer'],
        ]);

        $token = $user->createToken('auth_token')->plainTextToken;

//        return response()->json([
//            'access_token' => $token,
//            'token_type' => 'Bearer'
//        ]);

        $response = [
            'user'=>$user,
            'access_token'=>$token
        ];
        return response($response,201);
    }

    public function login(Request $request)
    {
        if (!Auth::attempt($request->only('email', 'password')))
        {
            return response()->json([
                'message' => 'Invalid login details'
            ], 401);
        }

        $user = User::where('email', $request['email'])->firstOrFail();

        $token = $user->createToken('auth_token')->plainTextToken;

        $response = [
            'user'=>$user,
            'access_token'=>$token
        ];
        return response($response,201);

//        return response()->json([
//            'access_token' => $token,
//            'token_type' => 'Bearer',
//        ]);
    }

    public function me(Request $request)
    {
        return $request->user();
    }

    public function logout(Request $request)
    {
        auth()->user()->tokens()->delete();

        return response([
            'message'=>'Logged out'
        ]);

    }


}
