<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\PermissionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

//Public routes
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);


//Protected routes
Route::group(['middleware' => 'auth:sanctum'], function(){
    Route::post('/me', [AuthController::class, 'me']);
    Route::post('/logout',[AuthController::class,'logout']);

    //  list of all tasks
    Route::get('tasks', [PermissionController::class, 'task']);
    // get task
    Route::get('tasks/{id}', [PermissionController::class, 'singleTask']);
    // create task
    Route::post('tasks', [PermissionController::class, 'createTask']);
    // update task
    Route::put('tasks/{id}', [PermissionController::class, 'updateTask']);
    // delete task
    Route::delete('tasks/{id}', [PermissionController::class, 'deleteTask']);
    //  create new customer with role Superuser
    Route::post('users/superuser', [PermissionController::class, 'createSuperuser']);
    // create new customer with role Customer
    Route::post('users/customer', [PermissionController::class, 'createCustomer']);
    // delete Superuser
    Route::delete('users/{id}', [PermissionController::class, 'deleteSuperuser']);
    // delete Customer
    Route::delete('users/customer/{id}', [PermissionController::class, 'deleteCustomer']);




});

